package com.example.kropp.mini_projekt22;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView msgTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        msgTextView = (TextView) findViewById(R.id.textView);

        IntentFilter filter = new IntentFilter("com.kropp.SEND_MESSAGE");
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg =  intent.getExtras().getString("msg");

                PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context.getPackageManager().getLaunchIntentForPackage("com.example.kropp.mini_projekt21")), 0);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
                mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                mBuilder.setContentTitle("Message get from 2.1 App:");
                mBuilder.setContentText(msg);
                mBuilder.setContentIntent(contentIntent);
                mBuilder.setAutoCancel(true);

                msgTextView.setText(msg.toString());

                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(0, mBuilder.build());
            }
        };

        registerReceiver(receiver, filter);


    }
}
